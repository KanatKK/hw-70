import React from 'react';
import './App.css';
import Menu from "../menu/menu";

const App = () => {
  return (
      <div className="container">
          <Menu/>
      </div>
  );
};

export default App;
