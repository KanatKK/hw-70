import React, {useEffect} from 'react';
import './menu.css';
import axios from 'axios';
import {useSelector, useDispatch} from "react-redux";
import {addDish, deleteDish, getDishes, showModal} from "../../store/actions";
import Modal from "../../components/modal/Modal";

const Menu = () => {
    const dispatch = useDispatch();
    const dishes = useSelector(state => state.get.dishes);
    const addedDishes = useSelector(state => state.add.addedDishes);

    useEffect(() => {
        const fetchData = async () => {
            const response = await axios.get('https://restaurant-b4f55.firebaseio.com/dishes.json');
            await dispatch(getDishes(response.data));
        };

        fetchData().catch(e => console.log(e));
    }, [dispatch]);

    const addDishes = async event => {
        const response = await axios.get('https://restaurant-b4f55.firebaseio.com/dishes/'+ event.target.value +'.json');
        dispatch(addDish(response.data));
    };

    const disabled = addedDishes === null;

    const deleteDishes = () => {
        dispatch(deleteDish());
    };

    const show = () => {
        dispatch(showModal());
    };

    const cartList = () => {
        if (addedDishes !== null) {
            return (
                    <div className="currentInfo">
                        <p className="name">{addedDishes.name}</p>
                        <span className="amount">X1</span>
                        <span className="price">{addedDishes.price} KGS</span>
                        <button className="deleteBtn" onClick={deleteDishes}>Delete</button>
                    </div>
            )
        }
    }
    if (dishes !== null) {
        return (
            <div className="menu">
                <Modal/>
                <div className="dishes">
                    {
                        Object.keys(dishes).map(item => {
                            return (<div className="dish" key={dishes[item].name}>
                                <img src={dishes[item].image} className="img" alt="dish"/>
                                <span className="dishInfo">
                                    <h3 className="dishName">{dishes[item].name}</h3>
                                    <p className="dishPrice">KGS {dishes[item].price}</p>
                                </span>
                                <button
                                    type="button" className="addBtn" value={item} onClick={addDishes}
                                >Add to cart</button>
                            </div>)})
                    }
                </div>
                <div className="cart">
                    <h2 className="cartHeading">Cart</h2>
                    <div className="info">
                        {cartList()}
                    </div>
                    <div className="total">
                        <p className="delivery">Delivery: 50 KGS</p>
                        <p className="totalPrice">Total price: {addedDishes && addedDishes.price + 50} KGS</p>
                    </div>
                    <button className="orderBtn" type="button" onClick={show} disabled={disabled}>Place order</button>
                </div>
            </div>
        );
    } else {
        return null;
    }
};
export default Menu;