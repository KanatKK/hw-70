export const GET_DISHES = 'GET_DISHES';
export const ADD_DISH = 'ADD_DISH';
export const DELETE_DISH = 'DELETE_DISH';
export const SHOW_MODAL = 'SHOW_MODAL';
export const CLOSE_MODAL = 'CLOSE_MODAL';