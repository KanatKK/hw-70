import {ADD_DISH, CLOSE_MODAL, DELETE_DISH, SHOW_MODAL} from "../actionTypes";

const initialState = {
    addedDishes: null,
    modal: 'none',
};

const addedDishes = (state = initialState, action) => {
    switch (action.type) {
        case ADD_DISH:
            return {...state, addedDishes: action.value};
        case DELETE_DISH:
            return {...state, addedDishes: null};
        case SHOW_MODAL:
            return {...state, modal: 'block'};
        case CLOSE_MODAL:
            return {
                ...state,
                modal: 'none',
                addedDishes: null,
            };
        default:
            return state;
    }
};

export default addedDishes