import {ADD_DISH, CLOSE_MODAL, DELETE_DISH, GET_DISHES, SHOW_MODAL} from "./actionTypes";

export const getDishes = value => {
    return {type: GET_DISHES, value};
};

export const addDish = value => {
    return {type: ADD_DISH, value};
};

export const deleteDish = () => {
    return {type: DELETE_DISH};
};

export const showModal = () => {
    return {type: SHOW_MODAL};
};

export const closeModal = () => {
    return {type: CLOSE_MODAL};
};