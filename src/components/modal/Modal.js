import React from 'react';
import './Modal.css';
import {useDispatch, useSelector} from "react-redux";
import {closeModal} from "../../store/actions";

const Modal = () => {
    const modal = useSelector(state => state.add.modal);
    const dispatch = useDispatch();

    const hideModal = () => {
        dispatch(closeModal());
    }

    return (
        <div className="modal" style={{display: modal}}>
            <input type="text" className="modalInputs" placeholder="Name"/>
            <input type="text" className="modalInputs" placeholder="Number"/>
            <input type="text" className="modalInputs" placeholder="Address"/>
            <button className="addBtn" onClick={hideModal} style={{marginLeft: 'auto', marginRight: 'auto'}}>Add</button>
        </div>
    );
};

export default Modal;